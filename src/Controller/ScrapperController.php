<?php
namespace App\Controller;

/**
 * Courses Controller
 *
 * @property \App\Model\Table\CoursesTable $Courses
 *
 * @method \App\Model\Entity\Course[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
use App\Controller\CoursesController;

class ScrapperController extends CoursesController
{
  public function index()
  {
    pr('Test');
    pr('scrapeVFX '.$this->scrapeVFX());
    pr('scrapeDSS '.$this->scrapeDSS());
    pr('scrapeEasysend '.$this->scrapeEasysend());
    $scrapeMoney=$this->scrapeMoney();
    pr('scrapeMoney averageRate '.($scrapeMoney['averageRate']));
    pr('scrapeMoney bidRate '.($scrapeMoney['bidRate']));
    pr('scrapeMoney askRate '.($scrapeMoney['askRate']));

    $test = [
      'money_cours' => 4.987,
      'dss_cours' => 4.9872
    ];
    pr($test);
    $this->add();
  }

// find web site
  public function scrapeMoney() // Znajdowqanie kursu Giełdy Money
    {
      $urlg = 'https://www.money.pl/u/data_window/?refresh=on&smb=GBPPLN';
  		$html_stringg = file_get_contents($urlg);
  		$patterng = '/rednia<\/td>\s*<td class="ar">.+<\/td>/';
  		preg_match($patterng, $html_stringg, $averageRate);
      $averageRate=$this->preperCours($averageRate);

      $patterng = '/\(bid\)<\/td>\s*<td class="ar">.+<\/td>/';
      preg_match($patterng, $html_stringg, $bidRate);
      $bidRate=$this->preperCours($bidRate);

      $patterng = '/\(ask\)<\/td>\s*<td class="ar">.+<\/td>/';
      preg_match($patterng, $html_stringg, $askRate);
      $askRate=$this->preperCours($askRate);

      $moneyRate = [
        'averageRate'=>$averageRate,
        'bidRate'=>$bidRate,
        'askRate'=>$askRate
      ];

      return($moneyRate);
    }

  public function scrapeVFX()  // Znajdowanie kursu VarciviaFX
    {
      $urlvfx = 'https://varsoviafx.co.uk/';
  		$html_stringvfx = file_get_contents($urlvfx);
  		$patternvfx = '/class="exchange-rate".+<\/span>/';
  		preg_match($patternvfx, $html_stringvfx, $kursvfx);
      $kvfx=$this->preperCours($kursvfx);

      return($kvfx);
    }

  public function scrapeDSS() // Znajdowqanie kursu Dla samych swoich
  {
    $urldss = 'https://www.przekazypieniezne.com/';
    $html_stringdss = file_get_contents($urldss);
    $patterndss = '/"exchange":".+?"/';
    preg_match($patterndss, $html_stringdss, $kursdss);
    $kursdss=$this->preperCours($kursdss);

    return($kursdss);
  }

  public function scrapeEasysend() // Znajdowqanie kursu Easysend
  {
    $urles = 'https://www.easysend.pl/api/v2/public/offers/GB/GBP/PL/PLN/50000';
		$html_stringes = file_get_contents($urles);
		$patternes = '/"rate":".+?"/';
		preg_match($patternes, $html_stringes, $kurses);
    $kurses=$this->preperCours($kurses);

    return($kurses);
  }


  public function preperCours($course)  // remov form find string onli digits
    {
      preg_match_all('/[0-9.,]/', implode($course), $tempResult);
      $tempResult[0][1]='.';
      $result = implode($tempResult[0]);
      return($result);
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null|void Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $course = $this->Courses->newEmptyEntity();
        pr($course);
        die();

        if ($this->request->is('post')) {
            $course = $this->Courses->patchEntity($course, $this->request->getData());
            pr($course);
            die();
            if ($this->Courses->save($course)) {
                $this->Flash->success(__('The course has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The course could not be saved. Please, try again.'));
        }
        $this->set(compact('course'));
    }

// save findet cours to DB
  // public function saveCoursToDB($test)
  // {
  //   INSERT INTO `rates` (`id`, `create_date`, `money_rate`, `dss_rate`, `dssg_rate`, `vfx_rate`, `es_rate`, '1stcontact_rate') VALUES
  //   (1812, '2017-12-11 22:42:13', 4.8901, 4.8901, 4.8901, 4.8904, 4.9004, 4.9024),
  //   (1813, '2017-12-11 22:43:46', 4.8901, 4.8901, 4.8904, 4.8904, 4.9004, 4.9024),
  //   (1814, '2017-12-11 22:44:06', 4.8901, 4.8901, 4.8904, 4.8904, 4.9004, 4.9024),
  //   (1815, '2017-12-11 22:49:07', 4.8901, 4.8901, 4.8904, 4.8904, 4.9004, 4.9024);
  // }

}
