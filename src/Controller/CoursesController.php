<?php
declare(strict_types=1);

namespace App\Controller;

use Cake\I18n\FrozenTime;
/**
 * Courses Controller
 *
 * @property \App\Model\Table\CoursesTable $Courses
 *
 * @method \App\Model\Entity\Course[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class CoursesController extends AppController
{
    /**
     * Index method
     *
     * @return \Cake\Http\Response|null|void Renders view
     */
    public function index()
    {
        $this->paginate = array(
          'limit' => 20,
          'order' => array( // sets a default order to sort by
            'Courses.id' => 'desc'
          )
        );
        $courses = $this->paginate($this->Courses);

        $this->set(compact('courses'));
    }

    public function chart()
    {
        $courses = $this->paginate($this->Courses);

        $this->set(compact('courses'));
    }

    public function run()
    {
      pr('Create date ' .$time = new FrozenTime());
      pr('scrapeVFX '.$this->scrapeVFX());
      pr('scrapeDSS '.$this->scrapeDSS());
      pr('scrapeEasysend '.$this->scrapeEasysend());
      $scrapeMoney=$this->scrapeMoney();
      pr('scrapeMoney averageRate '.($scrapeMoney['averageRate']));
      pr('scrapeMoney bidRate '.($scrapeMoney['bidRate']));
      pr('scrapeMoney askRate '.($scrapeMoney['askRate']));
      pr('1stcontact ' .$this->scrapeF1stcontact());
      pr('Azimo ' .$this->scrapeAzimo());
      pr('Grosik ' .$this->scrapeGrosik());
      pr('OpalTransfer ' .$this->scrapeOpalTransfer());
      pr('Tonio ' .$this->scrapeTonio());
      // pr('Transfer24 ' .$this->scrapeTransfer24());
      // pr('Trejdoo ' .$this->scrapeTrejdoo());

      $data = [
        'create_date' => $time = new FrozenTime(),
        'vfx_cours' => $this->scrapeVFX(),
        'dss_cours' => $this->scrapeDSS(),
        'es_cours' => $this->scrapeEasysend(),
        'moneyaverageRate_cours' => $scrapeMoney['averageRate'],
        'moneybidRate_cours' => $scrapeMoney['bidRate'],
        'moneyaskRate_cours' => $scrapeMoney['askRate'],
        'f1stcontact_cours' => $this->scrapeF1stcontact(),
        'azimo_cours' => $this->scrapeAzimo(),
        'grosik_cours' => $this->scrapeGrosik(),
        'opaltransfer_cours' => $this->scrapeOpalTransfer(),
        'tonio_cours' => $this->scrapeTonio(),
        // 'transfer24_cours' => $this->scrapeTransfer24(),
        // 'trejdoo_cours' => $this->scrapeTrejdoo(),
      ];

      $this->addFoundCourses($data);
    }


    // find web site
      public function scrapeMoney() // Znajdowqanie kursu Giełdy Money
        {
          $urlg = 'https://www.money.pl/u/data_window/?refresh=on&smb=GBPPLN';
      		$html_stringg = file_get_contents($urlg);
      		$patterng = '/rednia<\/td>\s*<td class="ar">.+<\/td>/';
      		preg_match($patterng, $html_stringg, $averageRate);
          $averageRate=$this->preperCours($averageRate);

          $patterng = '/\(bid\)<\/td>\s*<td class="ar">.+<\/td>/';
          preg_match($patterng, $html_stringg, $bidRate);
          $bidRate=$this->preperCours($bidRate);

          $patterng = '/\(ask\)<\/td>\s*<td class="ar">.+<\/td>/';
          preg_match($patterng, $html_stringg, $askRate);
          $askRate=$this->preperCours($askRate);

          $moneyRate = [
            'averageRate'=>$averageRate,
            'bidRate'=>$bidRate,
            'askRate'=>$askRate
          ];

          return($moneyRate);
        }

      public function scrapeVFX()  // Znajdowanie kursu VarciviaFX
        {
          $url = 'https://varsoviafx.co.uk/';
      		$html_string = file_get_contents($url);
          $kvfx = 0;
          if($html_string){
        		$patternvfx = '/class="exchange-rate".+<\/span>/';
        		preg_match($patternvfx, $html_string, $kursvfx);
            $kvfx=$this->preperCours($kursvfx);
          }

          return($kvfx);
        }

      public function scrapeDSS() // Znajdowqanie kursu Dla samych swoich
      {
        $url = 'https://www.przekazypieniezne.com/';
        $html_string = file_get_contents($url);
        $kvfx = 0;
        if($html_string){
          $patterndss = '/"exchange":".+?"/';
          preg_match($patterndss, $html_string, $kursdss);
          $kursdss=$this->preperCours($kursdss);
        }

        return($kursdss);
      }

      public function scrapeEasysend() // Znajdowqanie kursu Easysend
      {
        $url = 'https://www.easysend.pl/api/v2/public/offers/GB/GBP/PL/PLN/50000';
      	$html_string = file_get_contents($url);
        $kurses = 0;
        if($html_string){
      		$patternes = '/"rate":".+?"/';
      		preg_match($patternes, $html_string, $kurses);
          $kurses=$this->preperCours($kurses);
        }

        return($kurses);
      }

      public function scrapeF1stcontact() // Znajdowqanie kursu 1stcontact
      {
        $url = 'https://www.1stcontact.com/CurrencyToolkitController/CurrencyController.ashx?RequestType=getRates&FromCurrency=21&ToCurrency=112&Rate=5&Theme=3&Client=0';
        $html_string = file_get_contents($url);
        $f1stcontact = 0;
        if($html_string){
          $patternes = '/000,"BuyRate":[0-9.]{4,}/';
          preg_match($patternes, $html_string, $f1stcontact);
          $f1stcontact = substr($f1stcontact[0], 4);
          $f1stcontact = ([$f1stcontact]);
          $f1stcontact=$this->preperCours($f1stcontact);
        }

        return($f1stcontact);
      }

      public function scrapeAzimo() // Znajdowqanie kursu Azimo
      {
        $url = 'https://api.azimo.com/service-rates/v1/public/prices/current?sendingCountry=GBR&sendingCurrency=GBP&receivingCountry=POL&receivingCurrency=PLN&deliveryMethod=BANK_DEPOSIT';
        $html_string = file_get_contents($url);
        $azimo = 0;
        if($html_string){
          $patternes = '/"rate":".+?"/';
          preg_match($patternes, $html_string, $azimo);
          $azimo=$this->preperCours($azimo);
        }

        return($azimo);
      }

      public function scrapeGrosik() // Znajdowqanie kursu Grosik
      {
        $url = 'https://www.grosik.com/';
        $html_string = file_get_contents($url);
        $grosik = 0;
        if($html_string){
          $patternes = '/GBP<\/div><div class=rates-values>=.+?"/';
          preg_match($patternes, $html_string, $grosik);
          $grosik=$this->preperCours($grosik);
        }

        return($grosik);
      }

      public function scrapeOpalTransfer() // Znajdowqanie kursu OpalTransfer
      {
        $url = 'https://www.opaltransfer.com/pl';
        $html_string = file_get_contents($url);
        $opalTransfer = 0;
        if($html_string){
          $patternes = '/"PL":{"GBP":{"PLN":.+?}/';
          preg_match($patternes, $html_string, $opalTransfer);
          $opalTransfer=$this->preperCours($opalTransfer);
        }

        return($opalTransfer);
      }

      public function scrapeTonio() // Znajdowqanie kursu Tonio
      {
        $url = 'https://www.tonio.co.uk/strona/cennik';
        $html_string = file_get_contents($url);
        $tonio = 0;
        if($html_string){
          $patternes = '/gbppln"\s*value="\s.+?"/';
          preg_match($patternes, $html_string, $tonio);
          $tonio=$this->preperCours($tonio);
        }

        return($tonio);
      }

      public function scrapeTransfer24() // Znajdowqanie kursu Transfer24
      {
        $url = 'https://transfer24.eu/';
        $html_string = file_get_contents($url);
        var_dump($html_string);
        $transfer = 0;
        if($html_string){
          $patternes = '/var\sGBP\s=\s.+?;/';
          preg_match($patternes, $html_string, $transfer);
          $transfer=$this->preperCours($transfer);
        }

        return($transfer);
      }

      public function scrapeTrejdoo() // Znajdowqanie kursu Trejdoo
      {
        $url = 'http://www.trejdoo.com/analizy/kursy-walut/gbp-pln/';
        $html_string = file_get_contents($url);
        $trejdoo = 0;
        if($html_string){
          $patternes = '/uk-h1\suk-margin-top-remove">.+?</';
          preg_match($patternes, $html_string, $trejdoo);
          $trejdoo = substr($trejdoo[0], 7);
          $trejdoo = ([$trejdoo]);
          $trejdoo=$this->preperCours($trejdoo);
        }

        return($trejdoo);
      }


      public function preperCours($course)  // remov form find string onli digits
        {
          preg_match_all('/[0-9.,]/', implode($course), $tempResult);
          $tempResult[0][1]='.';
          $result = implode($tempResult[0]);
          return($result);
        }

      public function addFoundCourses($courses)
      {
          $course = $this->Courses->newEmptyEntity();
          $course = $this->Courses->patchEntity($course, $courses);

          if ($this->Courses->save($course)) {
              $this->Flash->success(__('The course has been saved.'));

              // return $this->redirect(['action' => 'index']);
          }
          $this->Flash->error(__('The course could not be saved. Please, try again.'));
          $this->set(compact('course'));
      }


    /**
     * View method
     *
     * @param string|null $id Course id.
     * @return \Cake\Http\Response|null|void Renders view
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $course = $this->Courses->get($id, [
            'contain' => [],
        ]);

        $this->set('course', $course);
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null|void Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $course = $this->Courses->newEmptyEntity();
        if ($this->request->is('post')) {
            $course = $this->Courses->patchEntity($course, $this->request->getData());
            if ($this->Courses->save($course)) {
                $this->Flash->success(__('The course has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The course could not be saved. Please, try again.'));
        }
        $this->set(compact('course'));
    }

    /**
     * Edit method
     *
     * @param string|null $id Course id.
     * @return \Cake\Http\Response|null|void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $course = $this->Courses->get($id, [
            'contain' => [],
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $course = $this->Courses->patchEntity($course, $this->request->getData());
            if ($this->Courses->save($course)) {
                $this->Flash->success(__('The course has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The course could not be saved. Please, try again.'));
        }
        $this->set(compact('course'));
    }

    /**
     * Delete method
     *
     * @param string|null $id Course id.
     * @return \Cake\Http\Response|null|void Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $course = $this->Courses->get($id);
        if ($this->Courses->delete($course)) {
            $this->Flash->success(__('The course has been deleted.'));
        } else {
            $this->Flash->error(__('The course could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }
}
