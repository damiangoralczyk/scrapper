<?php
declare(strict_types=1);

namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Course Entity
 *
 * @property int $id
 * @property \Cake\I18n\FrozenTime|null $create_date
 * @property string|null $vfx_cours
 * @property string|null $dss_cours
 * @property string|null $es_cours
 * @property string|null $moneyaverageRate_cours
 * @property string|null $moneybidRate_cours
 * @property string|null $moneyaskRate_cours
 * @property string|null $f1stcontact_cours
 * @property string|null $azimo_cours
 * @property string|null $grosik_cours
 * @property string|null $opaltransfer_cours
 * @property string|null $tonio_cours
 * @property string|null $transfer24_cours
 * @property string|null $trejdoo_cours
 */
class Course extends Entity
{
    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'create_date' => true,
        'vfx_cours' => true,
        'dss_cours' => true,
        'es_cours' => true,
        'moneyaverageRate_cours' => true,
        'moneybidRate_cours' => true,
        'moneyaskRate_cours' => true,
        'f1stcontact_cours' => true,
        'azimo_cours' => true,
        'grosik_cours' => true,
        'opaltransfer_cours' => true,
        'tonio_cours' => true,
        'transfer24_cours' => true,
        'trejdoo_cours' => true,
    ];
}
