<?php
declare(strict_types=1);

namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Courses Model
 *
 * @method \App\Model\Entity\Course newEmptyEntity()
 * @method \App\Model\Entity\Course newEntity(array $data, array $options = [])
 * @method \App\Model\Entity\Course[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\Course get($primaryKey, $options = [])
 * @method \App\Model\Entity\Course findOrCreate($search, ?callable $callback = null, $options = [])
 * @method \App\Model\Entity\Course patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\Course[] patchEntities(iterable $entities, array $data, array $options = [])
 * @method \App\Model\Entity\Course|false save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Course saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Course[]|\Cake\Datasource\ResultSetInterface|false saveMany(iterable $entities, $options = [])
 * @method \App\Model\Entity\Course[]|\Cake\Datasource\ResultSetInterface saveManyOrFail(iterable $entities, $options = [])
 * @method \App\Model\Entity\Course[]|\Cake\Datasource\ResultSetInterface|false deleteMany(iterable $entities, $options = [])
 * @method \App\Model\Entity\Course[]|\Cake\Datasource\ResultSetInterface deleteManyOrFail(iterable $entities, $options = [])
 */
class CoursesTable extends Table
{
    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config): void
    {
        parent::initialize($config);

        $this->setTable('courses');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator): Validator
    {
        $validator
            ->integer('id')
            ->allowEmptyString('id', null, 'create');

        $validator
            ->dateTime('create_date')
            ->allowEmptyDateTime('create_date');

        $validator
            ->decimal('vfx_cours')
            ->allowEmptyString('vfx_cours');

        $validator
            ->decimal('dss_cours')
            ->allowEmptyString('dss_cours');

        $validator
            ->decimal('es_cours')
            ->allowEmptyString('es_cours');

        $validator
            ->decimal('moneyaverageRate_cours')
            ->allowEmptyString('moneyaverageRate_cours');

        $validator
            ->decimal('moneybidRate_cours')
            ->allowEmptyString('moneybidRate_cours');

        $validator
            ->decimal('moneyaskRate_cours')
            ->allowEmptyString('moneyaskRate_cours');

        $validator
            ->decimal('f1stcontact_cours')
            ->allowEmptyString('f1stcontact_cours');

        $validator
            ->decimal('azimo_cours')
            ->allowEmptyString('azimo_cours');

        $validator
            ->decimal('grosik_cours')
            ->allowEmptyString('grosik_cours');

        $validator
            ->decimal('opaltransfer_cours')
            ->allowEmptyString('opaltransfer_cours');

        $validator
            ->decimal('tonio_cours')
            ->allowEmptyString('tonio_cours');

        $validator
            ->decimal('transfer24_cours')
            ->allowEmptyString('transfer24_cours');

        $validator
            ->decimal('trejdoo_cours')
            ->allowEmptyString('trejdoo_cours');

        return $validator;
    }
}
