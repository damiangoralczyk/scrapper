CREATE TABLE courses(
id INT(11) AUTO_INCREMENT PRIMARY KEY,
create_date DATETIME,
vfx_cours  DECIMAL(5,4),
dss_cours DECIMAL(5,4),
es_cours DECIMAL(5,4),
moneyaverageRate_cours DECIMAL(5,4),
moneybidRate_cours DECIMAL(5,4),
moneyaskRate_cours DECIMAL(5,4),
f1stcontact_cours  DECIMAL(7,6),
azimo_cours DECIMAL(6,5),
grosik_cours DECIMAL(5,4),
opaltransfer_cours DECIMAL(5,4),
tonio_cours DECIMAL(5,4),
transfer24_cours DECIMAL(5,4),
trejdoo_cours DECIMAL(5,4)
);

INSERT INTO `courses` (`id`, `create_date`, `vfx_cours`, `dss_cours`, `es_cours`, `moneyaverageRate_cours`, `moneybidRate_cours`, `moneyaskRate_cours`, `f1stcontact_cours`, `azimo_cours`, `grosik_cours`, `opaltransfer_cours`, `tonio_cours`, `transfer24_cours`, `trejdoo_cours`) VALUES
(1, '2017-12-11 22:42:13', 4.9261, 4.8901, 4.9497, 4.8904, 4.9072, 4.9024, 4.979032, 4.95778, 4.8904, 4.91, 4.83, 4.870, 4.8901),
(2, '2017-12-11 22:47:46', 4.9261, 4.8901, 4.9417, 4.8904, 4.9072, 4.9024, 4.971032, 4.99778, 4.8904, 4.90, 4.83, 4.860, 4.8901),
(3, '2017-12-11 22:51:06', 4.9201, 4.8901, 4.9427, 4.8904, 4.9004, 4.9024, 4.974032, 4.95778, 4.8884, 4.94, 4.83, 4.850, 4.9072),
(4, '2017-12-11 22:55:07', 4.9181, 4.8901, 4.9387, 4.8904, 4.9004, 4.9072, 4.979232, 4.85778, 4.8924, 4.90, 4.86, 4.870, 4.9072),
(5, '2017-12-11 22:59:17', 4.9201, 4.8901, 4.9497, 4.8904, 4.9004, 4.9072, 4.979932, 4.95578, 4.8984, 4.92, 4.88, 4.880, 4.8901),
(6, '2017-12-11 22:42:13', 4.9261, 4.8901, 4.9497, 4.8904, 4.9072, 4.9024, 4.979032, 4.95778, 4.8904, 4.91, 4.83, 4.870, 4.8901),
(7, '2017-12-11 22:47:46', 4.9261, 4.8901, 4.9417, 4.8904, 4.9072, 4.9024, 4.971032, 4.99778, 4.8904, 4.90, 4.83, 4.860, 4.8901),
(8, '2017-12-11 22:51:06', 4.9201, 4.8901, 4.9427, 4.8904, 4.9004, 4.9024, 4.974032, 4.95778, 4.8884, 4.94, 4.83, 4.850, 4.9072),
(9, '2017-12-11 22:55:07', 4.9181, 4.8901, 4.9387, 4.8904, 4.9004, 4.9072, 4.979232, 4.85778, 4.8924, 4.90, 4.86, 4.870, 4.9072),
(10, '2017-12-11 22:59:17', 4.9201, 4.8901, 4.9497, 4.8904, 4.9004, 4.9072, 4.979932, 4.95578, 4.8984, 4.92, 4.88, 4.880, 4.8901);

CREATE TABLE users(
id INT(11) AUTO_INCREMENT PRIMARY KEY,
create_date DATETIME,
username VARCHAR(65),
password VARCHAR(100),
type INT(1)
);

INSERT INTO `users` (`id`, `create_date`, `username`, `password`, `type`) VALUES
(1, '2017-12-11 22:42:13', 'admin', 'admin', 2),
(2, '2017-12-11 22:43:46', 'user', 'user', 1),
(3, '2017-12-11 22:44:06', 'gest', 'gest', 1);

CREATE TABLE `requests` (
`id` char(36) COLLATE utf8mb4_bin NOT NULL,
`url` text COLLATE utf8mb4_bin NOT NULL,
`content_type` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL,
`status_code` int(11) DEFAULT NULL,
`method` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL,
`requested_at` datetime NOT NULL,
PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin

CREATE TABLE `panels` (
`id` char(36) COLLATE utf8mb4_bin NOT NULL,
`request_id` char(36) COLLATE utf8mb4_bin NOT NULL,
`panel` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL,
`title` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL,
`element` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL,
`summary` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL,
`content` longblob DEFAULT NULL,
PRIMARY KEY (`id`),
UNIQUE KEY `unique_panel` (`request_id`,`panel`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin
