<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Course[]|\Cake\Collection\CollectionInterface $courses
 */
// header("Refresh:300");
?>
<div class="courses index content">
    <?= $this->Html->link(__('Index'), ['action' => 'index'], ['class' => 'button float-right']) ?>
    <?= $this->Html->link(__('Run'), ['action' => 'run'], ['class' => 'button float-right']) ?>
