<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Course $course
 */
?>
<div class="row">
    <aside class="column">
        <div class="side-nav">
            <h4 class="heading"><?= __('Actions') ?></h4>
            <?= $this->Html->link(__('Edit Course'), ['action' => 'edit', $course->id], ['class' => 'side-nav-item']) ?>
            <?= $this->Form->postLink(__('Delete Course'), ['action' => 'delete', $course->id], ['confirm' => __('Are you sure you want to delete # {0}?', $course->id), 'class' => 'side-nav-item']) ?>
            <?= $this->Html->link(__('List Courses'), ['action' => 'index'], ['class' => 'side-nav-item']) ?>
            <?= $this->Html->link(__('New Course'), ['action' => 'add'], ['class' => 'side-nav-item']) ?>
        </div>
    </aside>
    <div class="column-responsive column-80">
        <div class="courses view content">
            <h3><?= h($course->id) ?></h3>
            <table>
                <tr>
                    <th><?= __('Id') ?></th>
                    <td><?= $this->Number->format($course->id) ?></td>
                </tr>
                <tr>
                    <th><?= __('Vfx Cours') ?></th>
                    <td><?= $this->Number->format($course->vfx_cours) ?></td>
                </tr>
                <tr>
                    <th><?= __('Dss Cours') ?></th>
                    <td><?= $this->Number->format($course->dss_cours) ?></td>
                </tr>
                <tr>
                    <th><?= __('Es Cours') ?></th>
                    <td><?= $this->Number->format($course->es_cours) ?></td>
                </tr>
                <tr>
                    <th><?= __('MoneyaverageRate Cours') ?></th>
                    <td><?= $this->Number->format($course->moneyaverageRate_cours) ?></td>
                </tr>
                <tr>
                    <th><?= __('MoneybidRate Cours') ?></th>
                    <td><?= $this->Number->format($course->moneybidRate_cours) ?></td>
                </tr>
                <tr>
                    <th><?= __('MoneyaskRate Cours') ?></th>
                    <td><?= $this->Number->format($course->moneyaskRate_cours) ?></td>
                </tr>
                <tr>
                    <th><?= __('F1stcontact Cours') ?></th>
                    <td><?= $this->Number->format($course->f1stcontact_cours) ?></td>
                </tr>
                <tr>
                    <th><?= __('Azimo Cours') ?></th>
                    <td><?= $this->Number->format($course->azimo_cours) ?></td>
                </tr>
                <tr>
                    <th><?= __('Grosik Cours') ?></th>
                    <td><?= $this->Number->format($course->grosik_cours) ?></td>
                </tr>
                <tr>
                    <th><?= __('Opaltransfer Cours') ?></th>
                    <td><?= $this->Number->format($course->opaltransfer_cours) ?></td>
                </tr>
                <tr>
                    <th><?= __('Tonio Cours') ?></th>
                    <td><?= $this->Number->format($course->tonio_cours) ?></td>
                </tr>
                <tr>
                    <th><?= __('Transfer24 Cours') ?></th>
                    <td><?= $this->Number->format($course->transfer24_cours) ?></td>
                </tr>
                <tr>
                    <th><?= __('Trejdoo Cours') ?></th>
                    <td><?= $this->Number->format($course->trejdoo_cours) ?></td>
                </tr>
                <tr>
                    <th><?= __('Create Date') ?></th>
                    <td><?= h($course->create_date) ?></td>
                </tr>
            </table>
        </div>
    </div>
</div>
