<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Course[]|\Cake\Collection\CollectionInterface $courses
 */
?>
<div class="courses index content">
    <?= $this->Html->link(__('New Course'), ['action' => 'add'], ['class' => 'button float-right']) ?>
    <?= $this->Html->link(__('Run (download the current course)'), ['action' => 'run'], ['class' => 'button']) ?>
    <h3><?= __('Courses') ?></h3>
    <div class="table-responsive">
      <font size="1";>
        <table style="line-height: 1; letter-spacing: .0em;">
            <thead>
                <tr>
                    <th style="padding: 0.9rem 0.9rem;"><?= $this->Paginator->sort('id', 'ID') ?></th>
                    <th><?= $this->Paginator->sort('create_date', 'Create') ?></th>
                    <th style="padding: 0.9rem 0.9rem;"><?= $this->Paginator->sort('vfx_cours', 'VFX') ?></th>
                    <th style="padding: 0.9rem 0.9rem;"><?= $this->Paginator->sort('dss_cours', 'DSS') ?></th>
                    <th style="padding: 0.9rem 0.9rem;"><?= $this->Paginator->sort('es_cours', 'Easysend') ?></th>
                    <th style="padding: 0.9rem 0.9rem;"><?= $this->Paginator->sort('moneyaverageRate_cours', 'Money Average') ?></th>
                    <th style="padding: 0.9rem 0.9rem;"><?= $this->Paginator->sort('moneybidRate_cours', 'Money Bid') ?></th>
                    <th style="padding: 0.9rem 0.9rem;"><?= $this->Paginator->sort('moneyaskRate_cours', 'Money Ask') ?></th>
                    <th style="padding: 0.9rem 0.9rem;"><?= $this->Paginator->sort('f1stcontact_cours', '1stcontact') ?></th>
                    <th style="padding: 0.9rem 0.9rem;"><?= $this->Paginator->sort('azimo_cours', 'Azimo') ?></th>
                    <th style="padding: 0.9rem 0.9rem;"><?= $this->Paginator->sort('grosik_cours', 'Grosik') ?></th>
                    <th style="padding: 0.9rem 0.9rem;"><?= $this->Paginator->sort('opaltransfer_cours', 'OpalTransfer') ?></th>
                    <th style="padding: 0.9rem 0.9rem;"><?= $this->Paginator->sort('tonio_cours', 'Tonio') ?></th>
                    <th style="padding: 0.9rem 0.9rem;"><?= $this->Paginator->sort('transfer24_cours', 'Transfer24') ?></th>
                    <th style="padding: 0.9rem 0.9rem;"><?= $this->Paginator->sort('trejdoo_cours', 'Trejdoo') ?></th>
                    <th class="actions"><?= __('Actions') ?></th>
                </tr>
            </thead>
            <tbody>
                <?php foreach ($courses as $course): ?>
                <tr>
                    <td><?= $this->Number->format($course->id) ?></td>
                    <td><?= $this->Time->format($course->create_date, 'yyyy-MM-dd HH:mm:ss') ?></td>
                    <td><?= $this->Number->format($course->vfx_cours, ['precision' => 4]) ?></td>
                    <td><?= $this->Number->format($course->dss_cours, ['precision' => 4]) ?></td>
                    <td><?= $this->Number->format($course->es_cours, ['precision' => 4]) ?></td>
                    <td><?= $this->Number->format($course->moneyaverageRate_cours, ['precision' => 4]) ?></td>
                    <td><?= $this->Number->format($course->moneybidRate_cours, ['precision' => 4]) ?></td>
                    <td><?= $this->Number->format($course->moneyaskRate_cours, ['precision' => 4]) ?></td>
                    <td><?= $this->Number->format($course->f1stcontact_cours, ['precision' => 6]) ?></td>
                    <td><?= $this->Number->format($course->azimo_cours, ['precision' => 5]) ?></td>
                    <td><?= $this->Number->format($course->grosik_cours, ['precision' => 4]) ?></td>
                    <td><?= $this->Number->format($course->opaltransfer_cours, ['precision' => 4]) ?></td>
                    <td><?= $this->Number->format($course->tonio_cours, ['precision' => 4]) ?></td>
                    <td><?= $this->Number->format($course->transfer24_cours, ['precision' => 4]) ?></td>
                    <td><?= $this->Number->format($course->trejdoo_cours, ['precision' => 4]) ?></td>
                    <td class="actions">
                        <?= $this->Html->link(__('View'), ['action' => 'view', $course->id]) ?>
                        <?= $this->Html->link(__('Edit'), ['action' => 'edit', $course->id]) ?>
                        <?= $this->Form->postLink(__('Delete'), ['action' => 'delete', $course->id], ['confirm' => __('Are you sure you want to delete # {0}?', $course->id)]) ?>
                    </td>
                </tr>
                <?php endforeach; ?>
            </tbody>
        </table>
    </div>
    <div class="paginator">
        <ul class="pagination">
            <?= $this->Paginator->first('<< ' . __('first')) ?>
            <?= $this->Paginator->prev('< ' . __('previous')) ?>
            <?= $this->Paginator->numbers() ?>
            <?= $this->Paginator->next(__('next') . ' >') ?>
            <?= $this->Paginator->last(__('last') . ' >>') ?>
        </ul>
        <p><?= $this->Paginator->counter(__('Page {{page}} of {{pages}}, showing {{current}} record(s) out of {{count}} total')) ?></p>
    </div>
</div>
