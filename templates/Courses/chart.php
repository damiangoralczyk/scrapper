<?php echo $this->Html->script('plotly', array('inline'=>false));
header("Refresh:60");
/**
* @var \App\View\AppView $this
* @var \App\Model\Entity\Course[]|\Cake\Collection\CollectionInterface $courses
*/

use Cake\ORM\TableRegistry;
use Cake\I18n\FrozenTime;
?>
<html>
    <head>
<script>
    function getData() {
      return

      <?php
      $queryOneDay = TableRegistry::getTableLocator()->get('Courses')
      ->find()
      ->where(['create_date >=' => new FrozenTime('-1 day')])
      ;

      $dataX=null;
      $chartVfxCours=null;
      $chartMoneybidRateCours=null;

      foreach ($queryOneDay as $course) {
        $dataX.=$this->Time->format($course->create_date, '`yyyy-MM-dd HH:mm:ss`,');
        $chartVfxCours.=$course->vfx_cours.',';
        $chartDssCours.=$course->dss_cours.',';
        $chartEsCours.=$course->es_cours.',';
        $chartMoneybidRateCours.=$course->moneybidRate_cours.',';
      }

      $vfx_cours = $course->vfx_cours;
      $dss_cours = $course->dss_cours;
      $es_cours = $course->es_cours;
      $moneybidRate_cours = $course->moneybidRate_cours;

      $lastChangeVfxCours = 0;
      $lastChangeDssCours = 0;
      $lastChangeEsCours = 0;
      $lastChangeMoneybidRateCours = 0;

      foreach ($queryOneDay as $course) {
              if($vfx_cours-$course->vfx_cours !== 0.0000)
              {
                $lastChangeVfxCours = round($vfx_cours-$course->vfx_cours, 4);
              }
              if($dss_cours-$course->dss_cours !== 0.0000)
              {
                $lastChangeDssCours = round($dss_cours-$course->dss_cours, 4);
              }
              if($es_cours-$course->es_cours !== 0.0000)
              {
                $lastChangeEsCours = round($es_cours-$course->es_cours, 4);
              }
              if($moneybidRate_cours-$course->moneybidRate_cours !== 0.0000)
              {
                $lastChangeMoneybidRateCours = round($moneybidRate_cours-$course->moneybidRate_cours, 4);
              }
            }
      $queryOneHour = TableRegistry::getTableLocator()->get('Courses')
      ->find()
      ->where(['create_date >=' => new FrozenTime('-1 hour')])
      ->limit(1)
      ;
      foreach ($queryOneHour as $courseOneHour) {
                $beforeOneHourVfxCours = round($courseOneHour->vfx_cours-$vfx_cours, 4);
                $beforeOneHourDssCours = round($courseOneHour->dss_cours-$dss_cours, 4);
                $beforeOneHourEsCours = round($courseOneHour->es_cours-$es_cours, 4);
                $beforeOneHourMoneybidRateCours = round($courseOneHour->moneybidRate_cours-$moneybidRate_cours, 4);
            };
      ?>;

    }
</script>
	<div id='myDiv'><!-- Plotly chart will be drawn inside this DIV --></div>
    <meta http-equiv="content-type" content="text/html; charset=UTF-8" />
    </head>
    <body>
      <h1>
      <table>
        <style>
        td   {font-weight: bold;}
        td.negative { color : red; }
        td.positive { color : green; }
        </style>
        <tr>
          <th>Czas</th>
          <th>VFX</th>
          <th>DSS</th>
          <th>Easy Send</th>
          <th>Money</th>
        </tr>
        <tr>
          <td><?php echo $this->Time->format($course->create_date, 'HH:mm'); ?></td>
          <td style=color:black;><?php echo $course->vfx_cours; ?></td>
          <td style=color:black;><?php echo $course->dss_cours; ?></td>
          <td style=color:black;><?php echo $course->es_cours; ?></td>
          <td style=color:black;><?php echo $course->moneybidRate_cours; ?></td>
        </tr>
        <tr>
          <td style="font-size:22px">Ostatnia zmiana</td>
          <td class="plusmin"><?php echo $lastChangeVfxCours; ?></td>
          <td class="plusmin"><?php echo $lastChangeDssCours; ?></td>
          <td class="plusmin"><?php echo $lastChangeEsCours; ?></td>
          <td class="plusmin"><?php echo $lastChangeMoneybidRateCours; ?></td>
        </tr>
        <tr>
          <td style="font-size:22px">Kurs godzinę temu</td>
          <td class="plusmin"><?php echo $beforeOneHourVfxCours; ?></td>
          <td class="plusmin"><?php echo $beforeOneHourDssCours; ?></td>
          <td class="plusmin"><?php echo $beforeOneHourEsCours; ?></td>
          <td class="plusmin"><?php echo $beforeOneHourMoneybidRateCours; ?></td>
      </tr>
      <body>
      </table>
    </h1>
    <div class="wrapper" style="position: absolute; height: 100%; width: 100%;">
        <div id="chart"></div>
        <script>
          Plotly.plot('chart',[{
            x:[<?php echo $dataX ?>],
            y:[<?php echo $chartMoneybidRateCours ?>],
            type:'line',
            name: 'Money',
            line: {
              color: 'rgb(0, 0, 0)',
              width: 3
            }
          }]);
          Plotly.plot('chart',[{
              x:[<?php echo $dataX ?>],
              y:[<?php echo $chartVfxCours ?>],
              type:'line',
              name: 'Vfx',
              line: {
                color: 'rgb(53, 180, 0)',
                width: 3
              }
          }]);
          Plotly.plot('chart',[{
              x:[<?php echo $dataX ?>],
              y:[<?php echo $chartDssCours ?>],
              type:'line',
              name: 'Dss',
              line: {
                color: 'rgb(255, 27, 116)',
                width: 3
              }
          }]);
          Plotly.plot('chart',[{
              x:[<?php echo $dataX ?>],
              y:[<?php echo $chartEsCours ?>],
              type:'line',
              name: 'Es',
              line: {
                color: 'rgb(0, 56, 224)',
                width: 3
              }
          }]);

          function MakePosNeg() {
            var TDs = document.querySelectorAll('.plusmin');

            for (var i = 0; i < TDs.length; i++) {
              var temp = TDs[i];
              if (temp.firstChild.nodeValue.indexOf('-') == 0) {temp.className = "negative";}
              else {temp.className = "positive";}
            }
          }
          onload = MakePosNeg()
        </script>
    </div>
    </body>
</html>
