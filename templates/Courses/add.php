<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Course $course
 */
?>
<div class="row">
    <aside class="column">
        <div class="side-nav">
            <h4 class="heading"><?= __('Actions') ?></h4>
            <?= $this->Html->link(__('List Courses'), ['action' => 'index'], ['class' => 'side-nav-item']) ?>
        </div>
    </aside>
    <div class="column-responsive column-80">
        <div class="courses form content">
            <?= $this->Form->create($course) ?>
            <fieldset>
                <legend><?= __('Add Course') ?></legend>
                <?php
                    echo $this->Form->control('create_date', ['empty' => true]);
                    echo $this->Form->control('vfx_cours');
                    echo $this->Form->control('dss_cours');
                    echo $this->Form->control('es_cours');
                    echo $this->Form->control('moneyaverageRate_cours');
                    echo $this->Form->control('moneybidRate_cours');
                    echo $this->Form->control('moneyaskRate_cours');
                    echo $this->Form->control('f1stcontact_cours');
                    echo $this->Form->control('azimo_cours');
                    echo $this->Form->control('grosik_cours');
                    echo $this->Form->control('opaltransfer_cours');
                    echo $this->Form->control('tonio_cours');
                    echo $this->Form->control('transfer24_cours');
                    echo $this->Form->control('trejdoo_cours');
                ?>
            </fieldset>
            <?= $this->Form->button(__('Submit')) ?>
            <?= $this->Form->end() ?>
        </div>
    </div>
</div>
