<?php
use Cake\I18n\FrozenTime;

/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\User $user
 */
?>
<div class="row">
    <aside class="column">
        <div class="side-nav">
            <h4 class="heading"><?= __('Actions') ?></h4>
            <?= $this->Html->link(__('List Users'), ['action' => 'index'], ['class' => 'side-nav-item']) ?>
        </div>
    </aside>
    <div class="column-responsive column-80">
        <div class="users form content">
            <?= $this->Form->create($user) ?>
            <fieldset>
                <legend><?= __('Add User') ?></legend>
                <?php
                    echo $this->Form->control('create_date' ,['required' => false, 'empty' => false, 'default' => new FrozenTime()]);
                    echo $this->Form->control('username', ['required' => true]);
                    echo $this->Form->control('password', ['required' => true]);
                    echo 'Type';
                    echo $this->Form->select('type', [1 => 'User', 2 => 'Admin'], ['empty' => false, 'required' => true, 'default' => 1]);
                ?>
            </fieldset>
            <?= $this->Form->button(__('Submit')) ?>
            <?= $this->Form->end() ?>
        </div>
    </div>
</div>
