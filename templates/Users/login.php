<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\User $user
 */
?>

<?= $this->Form->create() ?>
<?= $this->Form->control('username', ['label'=>'Username','placeholder'=>'Enter the username','reqired'=>true]) ?>
<?= $this->Form->control('password', ['label'=>'Password','placeholder'=>'Enter the password','reqired'=>true]) ?>
<?= $this->Form->submit(_('Login'),['class'=>'button']) ?>
<?= $this->Form->end() ?>
