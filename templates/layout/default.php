<?php
/**
 * CakePHP(tm) : Rapid Development Framework (https://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 * @link          https://cakephp.org CakePHP(tm) Project
 * @since         0.10.0
 * @license       https://opensource.org/licenses/mit-license.php MIT License
 * @var \App\View\AppView $this
 */

$cakeDescription = 'Scrapperr';
?>
<!DOCTYPE html>
<html>
<head>
    <?= $this->Html->charset() ?>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>
        <?= $cakeDescription ?>:
        <?= $this->fetch('title') ?>
    </title>
    <?= $this->Html->meta('icon') ?>

    <link href="https://fonts.googleapis.com/css?family=Raleway:400,700" rel="stylesheet">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/normalize.css@8.0.1/normalize.css">

    <?= $this->Html->css('milligram.min.css') ?>
    <?= $this->Html->css('cake.css') ?>

    <?= $this->fetch('meta') ?>
    <?= $this->fetch('css') ?>
    <?= $this->fetch('script') ?>
</head>
<body>
    <nav class="top-nav">
        <div class="top-nav-title">
            <a><span>Scrapperr</span></a>
            <?= $this->Html->link(__('Courses Chart'), ['controller'=>'Courses','action' => 'Chart'], ['class' => 'button-clear']) ?>
        </div>
        <!-- <?= $this->Html->link(__('Dashboard'), ['controller'=>'Courses','action' => 'Chart'], ['class' => 'button float-right']) ?> -->
        <div class="top-nav-links">
          <?php
                $currentuser = $this->getRequest()->getSession()->read('Auth.User');
                if(isset($currentuser['username'])){
                  echo $this->Html->link(__('Logout'), ['controller'=>'Users','action' => 'Logout'], ['class' => 'button float-right']);
                  echo $this->Html->link(__('Dashboard'), ['controller'=>'Courses','action' => 'Chart'], ['class' => 'button float-right']);
                  echo $this->Html->link(__('Users'), ['controller'=>'Users','action' => 'Index'], ['class' => 'button-clear']);
                  echo $this->Html->link(__('Courses'), ['controller'=>'Courses','action' => 'Index'], ['class' => 'button-clear']);
                }
                else{
                  if (strpos($_SERVER['REQUEST_URI'], 'login') == false){
                    echo $this->Html->link(__('Login'), ['controller'=>'Users','action' => 'Login'], ['class' => 'button float-right']);
                  }else {
                    echo $this->Html->link(__('Dashboard'), ['controller'=>'Courses','action' => 'Chart'], ['class' => 'button float-right']);
                  }
                }
          ?>
        </div>
    </nav>
    <main class="main">
        <div class="container">
            <?= $this->Flash->render() ?>
            <?= $this->fetch('content') ?>
        </div>
    </main>
    <footer>
    </footer>
</body>
</html>
