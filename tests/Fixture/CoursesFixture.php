<?php
declare(strict_types=1);

namespace App\Test\Fixture;

use Cake\TestSuite\Fixture\TestFixture;

/**
 * CoursesFixture
 */
class CoursesFixture extends TestFixture
{
    /**
     * Fields
     *
     * @var array
     */
    // phpcs:disable
    public $fields = [
        'id' => ['type' => 'integer', 'length' => 11, 'unsigned' => false, 'null' => false, 'default' => null, 'comment' => '', 'autoIncrement' => true, 'precision' => null],
        'create_date' => ['type' => 'datetime', 'length' => null, 'precision' => null, 'null' => true, 'default' => null, 'comment' => ''],
        'vfx_cours' => ['type' => 'decimal', 'length' => 5, 'precision' => 4, 'unsigned' => false, 'null' => true, 'default' => null, 'comment' => ''],
        'dss_cours' => ['type' => 'decimal', 'length' => 5, 'precision' => 4, 'unsigned' => false, 'null' => true, 'default' => null, 'comment' => ''],
        'es_cours' => ['type' => 'decimal', 'length' => 5, 'precision' => 4, 'unsigned' => false, 'null' => true, 'default' => null, 'comment' => ''],
        'moneyaverageRate_cours' => ['type' => 'decimal', 'length' => 5, 'precision' => 4, 'unsigned' => false, 'null' => true, 'default' => null, 'comment' => ''],
        'moneybidRate_cours' => ['type' => 'decimal', 'length' => 5, 'precision' => 4, 'unsigned' => false, 'null' => true, 'default' => null, 'comment' => ''],
        'moneyaskRate_cours' => ['type' => 'decimal', 'length' => 5, 'precision' => 4, 'unsigned' => false, 'null' => true, 'default' => null, 'comment' => ''],
        'f1stcontact_cours' => ['type' => 'decimal', 'length' => 7, 'precision' => 6, 'unsigned' => false, 'null' => true, 'default' => null, 'comment' => ''],
        'azimo_cours' => ['type' => 'decimal', 'length' => 6, 'precision' => 5, 'unsigned' => false, 'null' => true, 'default' => null, 'comment' => ''],
        'grosik_cours' => ['type' => 'decimal', 'length' => 5, 'precision' => 4, 'unsigned' => false, 'null' => true, 'default' => null, 'comment' => ''],
        'opaltransfer_cours' => ['type' => 'decimal', 'length' => 5, 'precision' => 4, 'unsigned' => false, 'null' => true, 'default' => null, 'comment' => ''],
        'tonio_cours' => ['type' => 'decimal', 'length' => 5, 'precision' => 4, 'unsigned' => false, 'null' => true, 'default' => null, 'comment' => ''],
        'transfer24_cours' => ['type' => 'decimal', 'length' => 5, 'precision' => 4, 'unsigned' => false, 'null' => true, 'default' => null, 'comment' => ''],
        'trejdoo_cours' => ['type' => 'decimal', 'length' => 5, 'precision' => 4, 'unsigned' => false, 'null' => true, 'default' => null, 'comment' => ''],
        '_constraints' => [
            'primary' => ['type' => 'primary', 'columns' => ['id'], 'length' => []],
        ],
        '_options' => [
            'engine' => 'InnoDB',
            'collation' => 'utf8mb4_bin'
        ],
    ];
    // phpcs:enable
    /**
     * Init method
     *
     * @return void
     */
    public function init(): void
    {
        $this->records = [
            [
                'id' => 1,
                'create_date' => '2020-03-29 19:12:05',
                'vfx_cours' => 1.5,
                'dss_cours' => 1.5,
                'es_cours' => 1.5,
                'moneyaverageRate_cours' => 1.5,
                'moneybidRate_cours' => 1.5,
                'moneyaskRate_cours' => 1.5,
                'f1stcontact_cours' => 1.5,
                'azimo_cours' => 1.5,
                'grosik_cours' => 1.5,
                'opaltransfer_cours' => 1.5,
                'tonio_cours' => 1.5,
                'transfer24_cours' => 1.5,
                'trejdoo_cours' => 1.5,
            ],
        ];
        parent::init();
    }
}
